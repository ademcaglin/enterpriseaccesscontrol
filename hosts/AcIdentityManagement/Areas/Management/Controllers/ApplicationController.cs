﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AccessControl;
using AcIdentityManagement.Infrastructure;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace AcIdentityManagement.Areas.Management.Controllers
{
    public class ApplicationController : AcApiController
    {
        public IActionResult Index()
        {
            var resource = "MANAGEMENT";
            if (!IsResourceAuthorized(resource))
            {
                return new ChallengeResult();
            }
            return null;
        }
    }
}
