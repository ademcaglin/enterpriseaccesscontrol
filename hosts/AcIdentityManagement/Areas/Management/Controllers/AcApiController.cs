﻿using AccessControl;
using AcIdentityManagement.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AcIdentityManagement.Areas.Management.Controllers
{
    [Area(AcScopes.Management)]
    [AcAuthorize(Scope = AcScopes.Management, Resource = AcResourceTypes.Application)]
    public class ManagementApiController : AcApiController
    {

    }
}
