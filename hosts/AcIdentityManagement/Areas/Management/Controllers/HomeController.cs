﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using AcIdentityManagement.Infrastructure;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace AcIdentityManagement.Areas.Management.Controllers
{
    [Area(AcScopes.Management)]
    public class HomeController : Controller
    {
        [Authorize(ActiveAuthenticationSchemes = "oidc")]
        public IActionResult Index()
        {
            return View();
        }
    }
}
