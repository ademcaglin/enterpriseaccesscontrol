﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace AcIdentityManagement.Controllers
{
    public class HomeController : Controller
    {
        [Authorize(ActiveAuthenticationSchemes = "oidc")]
        public IActionResult Index()
        {
            return View();
        }
    }
}
