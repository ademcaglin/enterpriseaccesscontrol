﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AcIdentityManagement.Infrastructure
{
    public class AcScopes
    {
        public const string Administration = "ADMINISTRATION";

        public const string Management = "MANAGEMENT";
    }
}
