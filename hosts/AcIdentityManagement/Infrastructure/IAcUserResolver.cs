﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AcIdentityManagement.Infrastructure
{
    public interface IAcUserResolver
    {
        string GetUserId();
    }
}
