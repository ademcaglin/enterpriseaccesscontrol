﻿using System.ComponentModel.DataAnnotations;

namespace AccessControl.Core.Resources
{
    public enum ResourceStatus
    {
        [Display(Name = "New")]
        New = 0,
        [Display(Name = "Active")]
        Enabled = 1,
        [Display(Name = "Passive")]
        Disabled = 2
    }
}
