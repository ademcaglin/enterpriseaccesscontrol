﻿using AccessControl.Core.ResourceTypes;
using System.ComponentModel.DataAnnotations;

namespace AccessControl.Core.Resources
{
    /// <summary>
    /// Manages by Admin
    /// Ex: Name : Region, Value : Asia, NameTitle: Bölge, ValueTitle: Asya, FullTitle: Asya Bölgesi
    /// </summary>
    public class Resource : CreateAuditedEntity<int>
    {
        [Required]
        [StringLength(200)]
        public string Value { get; set; }

        [Required]
        [StringLength(200)]
        public string ValueTitle { get; set; }

        [Required]
        [StringLength(500)]
        public string FullTitle { get; set; }

        public ResourceStatus Status { get; set; }

        public ResourceType Type { get; set; }

        public void Enable()
        {
            Check.True(Status != ResourceStatus.Enabled);
            Status = ResourceStatus.Enabled;
        }

        public void Disable()
        {
            Check.True(Status == ResourceStatus.Enabled);
            Status = ResourceStatus.Disabled;
        }

        public bool CanDelete()
        {
            return Status == ResourceStatus.New;
        }
        public bool CanEdit()
        {
            return Status == ResourceStatus.New;
        }
        public bool CanEnable()
        {
            return Status == ResourceStatus.New;
        }
        public bool CanDisable()
        {
            return Status == ResourceStatus.Enabled;
        }
    }
}
