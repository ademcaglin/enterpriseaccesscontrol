﻿
using AccessControl.Core.Permissions;

namespace AccessControl.Core.Roles
{
    public class RolePermission : CreateAuditedEntity<int>
    {
        public Role Role { get; set; }

        public Permission Permission { get; set; }
    }
}
