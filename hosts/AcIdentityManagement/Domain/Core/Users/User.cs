﻿using System.Collections.Generic;

namespace AccessControl.Core.Users
{
    public class User : CreateAuditedEntity<string>
    {
        public string UserName { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public virtual ICollection<UserRole> Roles { get; } = new List<UserRole>();

        public bool IsActive { get; set; }

        public override string ToString()
        {
            return UserName;
        }
    }
}
