﻿
using AccessControl.Core.Resources;
using AccessControl.Core.Roles;

namespace AccessControl.Core.Users
{
    public class UserRole : CreateAuditedEntity<long>
    {
        public virtual User User { get; set; }

        public virtual Role Role { get; set; }

        public virtual Resource Resource { get; set; }

        public UserRoleStatus Status { get; set; }
    }
}
