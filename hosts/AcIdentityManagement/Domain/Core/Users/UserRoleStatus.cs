﻿
using System.ComponentModel.DataAnnotations;

namespace AccessControl.Core.Users
{
    public enum UserRoleStatus
    {
        [Display(Name = "New")]
        New=0,
        [Display(Name = "Active")]
        Enabled = 1,
        [Display(Name = "Passive")]
        Disabled = 2
    }
}
