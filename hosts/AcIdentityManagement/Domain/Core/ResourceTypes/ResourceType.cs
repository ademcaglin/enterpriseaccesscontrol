﻿using System.ComponentModel.DataAnnotations;

namespace AccessControl.Core.ResourceTypes
{
    /// <summary>
    /// Manages by user 
    /// </summary>
    public class ResourceType : CreateAuditedEntity<int>
    {
        [Required]
        [StringLength(200)]
        public string Name { get; set; }

        [Required]
        [StringLength(200)]
        public string Title { get; set; }
    }
}
