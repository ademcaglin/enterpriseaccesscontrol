﻿using AccessControl.Core.Modules;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AccessControl.Core.PermissionTypes
{
    /// <summary>
    /// Manages by user 
    /// </summary>
    public class PermissionType : CreateAuditedEntity<int>
    {
        [Required]
        [StringLength(200)]
        public string Name { get; set; }

        [Required]
        [StringLength(200)]
        public string Title { get; set; }

        [Required]
        [StringLength(500)]
        public string Description { get; set; }

        public virtual Scope Scope { get; set; }
    }
}
