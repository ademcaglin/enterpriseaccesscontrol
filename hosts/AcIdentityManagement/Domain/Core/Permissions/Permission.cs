﻿using AccessControl.Core.Modules;
using AccessControl.Core.PermissionTypes;
using System;
using System.ComponentModel.DataAnnotations;

namespace AccessControl.Core.Permissions
{
    public class Permission : CreateAuditedEntity<int>
    {
        [Required]
        [StringLength(200)]
        public string Name { get; set; }

        [StringLength(200)]
        public string Title { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        public bool IsActive { get; set; }

        public virtual PermissionType Type { get; set; }

        public virtual Scope Scope { get; set; }
    }
}
