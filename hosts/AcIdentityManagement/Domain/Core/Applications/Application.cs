﻿using AccessControl.Core.Modules;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AccessControl.Core.Applications
{
    /// <summary>
    /// Manage by Admin
    /// Ex: Name : RegionManagement, Title : Bölge Yönetimi, Description : ...
    /// </summary>
    public class Application : CreateAuditedEntity<string>
    {
        [Required]
        [StringLength(200)]
        public string Title { get; set; }

        [Required]
        [StringLength(500)]
        public string Description { get; set; }

        public ApplicationStatus Status { get; set; }

        public virtual ICollection<Scope> Scopes { get; } = new List<Scope>();

        public void Enable()
        {
            Check.True(Status != ApplicationStatus.Enabled);
            Status = ApplicationStatus.Enabled;
        }

        public void Disable()
        {
            Check.True(Status == ApplicationStatus.Enabled);
            Status = ApplicationStatus.Disabled;
        }

        public bool CanDelete()
        {
            return Status == ApplicationStatus.New;
        }

        public bool CanEdit()
        {
            return Status == ApplicationStatus.New;
        }

        public bool CanEnable()
        {
            return Status == ApplicationStatus.New;
        }

        public bool CanDisable()
        {
            return Status == ApplicationStatus.Enabled;
        }
    }
}
