﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using AccessControl.Data;
using AccessControl.Core.Permissions;

namespace AccessControl.Services
{
    public class AcApplicationService : IAcApplicationService
    {
        private readonly DbContextOptions<AcDbContext> _acDbContextOtions;
        public AcApplicationService(DbContextOptions<AcDbContext> acDbContextOtions)
        {
            _acDbContextOtions = acDbContextOtions;
        }

        public void SyncClaims(string applicationId, IEnumerable<AcPermission> permissions)
        {
            using (var _dbContext = new AcDbContext(null, _acDbContextOtions))
            {
                var app = _dbContext.Applications
                    .Include(x => x.Scopes).ThenInclude(y => y.Permissions)
                    .Include(x => x.Scopes).ThenInclude(y => y.Roles)
                    .SingleOrDefault(x => x.Id == applicationId);
                if (app == null || app.Status != Core.Applications.ApplicationStatus.Enabled)
                {
                    throw new ArgumentNullException("Application", "There is no application named '" + applicationId + "'");
                }
                foreach(var scope in app.Scopes)
                {
                    var requireDisableList = scope.Permissions.Where(x => 
                       x.IsActive &&
                       !permissions.Any(y => y.Name == x.Name))
                       .ToList();
                    foreach (var requireDisable in requireDisableList)
                    {
                        requireDisable.IsActive = false;
                    }
                    foreach (var permInput in permissions.Where(x=>x.Scope == scope.Name))
                    {
                        var exist = scope.Permissions
                            .FirstOrDefault(x => x.Name == permInput.Name);
                        if (exist == null)
                        {
                            var perm = new Permission();
                            perm.IsActive = true;
                            perm.Scope = scope;
                            perm.Name = permInput.Name;
                            perm.CreatedBy = "SYSTEM";
                            scope.Permissions.Add(perm);
                        }
                        else if (!exist.IsActive)
                        {
                            exist.IsActive = true;
                        }
                    }
                }
                _dbContext.SaveChanges();
            }
        }
    }
}
