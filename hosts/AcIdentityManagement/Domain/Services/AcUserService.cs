﻿using AccessControl.Core;
using AccessControl.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccessControl.Services
{
    public class AcUserService : IAcUserService
    {
        private readonly AcDbContext _dbContext;
        public AcUserService(AcDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<AcUserResult> GetUser(string applicationId, string userId)
        {
            var result = new AcUserResult();
            var user = await _dbContext.Users
                .Include(x => x.Roles).ThenInclude(y => y.Role).ThenInclude(z => z.Scope).ThenInclude(t => t.Application)
                .Include(x => x.Roles).ThenInclude(y => y.Role).ThenInclude(z => z.Scope).ThenInclude(t => t.ResourceType)
                .Include(x => x.Roles).ThenInclude(y => y.Role).ThenInclude(z => z.Scope).ThenInclude(t => t.Permissions).ThenInclude(u => u.Type)
                .Include(x => x.Roles).ThenInclude(y => y.Role).ThenInclude(z => z.Permissions)
                .Include(x => x.Roles).ThenInclude(y => y.Resource)
                .SingleOrDefaultAsync(x => x.Id == userId);
  
            var userRoles = user.Roles.Where(x =>
                x.Status == Core.Users.UserRoleStatus.Enabled &&
                x.Role.Status == Core.Roles.RoleStatus.Enabled &&
                x.Role.Scope.Application.Id == applicationId &&
                x.Role.Scope.IsActive)
               .ToList();
            foreach (var userRole in userRoles)
            {
                if (!result.Scopes.Any(x => 
                   x.Name == userRole.Role.Scope.Name &&
                   x.ResourceValue == userRole.Resource?.Value))
                {
                    var scope = new AcScope();
                    scope.Name = userRole.Role.Scope.Name;
                    scope.Title = userRole.Role.Scope.Name;
                    scope.ResourceTitle = userRole.Role.Scope.ResourceType?.Title;
                    scope.ResourceValue = userRole.Resource?.Value;
                    scope.ResourceName = userRole.Resource?.Type.Name;
                    scope.Permissions = userRoles.Where(x => scope.Name == x.Role.Scope.Name)
                        .SelectMany(x => x.Role.Permissions)
                        .Select(y => y.Permission.Name);
                    result.Scopes.Add(scope);
                }
            }
            result.Success = true;
            return result;
        }
    }
}
