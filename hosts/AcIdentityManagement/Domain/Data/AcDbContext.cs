﻿using Microsoft.EntityFrameworkCore;
using AccessControl.Core;
using System;
using System.Linq;
using AccessControl.Core.PermissionTypes;
using AccessControl.Core.Applications;
using AccessControl.Core.Users;
using AccessControl.Core.Permissions;
using AccessControl.Core.Roles;
using AccessControl.Core.Modules;
using AccessControl.Core.Resources;
using AccessControl.Core.ResourceTypes;
using AcIdentityManagement.Infrastructure;

namespace AccessControl.Data
{
    public class AcDbContext : DbContext
    {
        private readonly IAcUserResolver _acUserResolver;

        public AcDbContext()
        {
        }

        public AcDbContext(IAcUserResolver acUserResolver, DbContextOptions options)
            : base(options)
        {
            _acUserResolver = acUserResolver;
        }

        public DbSet<Application> Applications { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<Permission> Permissions { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Scope> Modules { get; set; }

        public DbSet<Resource> Resources { get; set; }

        public DbSet<ResourceType> ResourceTypes { get; set; }

        public DbSet<PermissionType> PermissionTypes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Scope>()
                   .HasOne(x => x.Application)
                   .WithMany(x => x.Scopes);

            modelBuilder.Entity<Permission>()
                   .HasOne(x => x.Scope)
                   .WithMany(x => x.Permissions);

            modelBuilder.Entity<Resource>()
                   .HasOne(x => x.Type);

            modelBuilder.Entity<Permission>()
                   .HasOne(x => x.Type);

            modelBuilder.Entity<Role>()
                   .HasOne(x => x.Scope)
                   .WithMany(x => x.Roles);

            modelBuilder.Entity<RolePermission>()
                  .HasOne(x => x.Role)
                  .WithMany(x => x.Permissions);

            modelBuilder.Entity<UserRole>()
                   .HasOne(x => x.User)
                   .WithMany(x => x.Roles);

            modelBuilder.Entity<UserRole>()
                  .HasOne(x => x.Role);
        }

        public override int SaveChanges()
        {
            
            var addedAuditedEntities = ChangeTracker.Entries<ICreateAuditedEntity>()
               .Where(p => p.State == EntityState.Added)
               .Select(p => p.Entity);

            var now = DateTime.Now;

            foreach (var added in addedAuditedEntities)
            {
                added.CreatedAt = now;
                if (_acUserResolver != null)
                {
                    added.CreatedBy = _acUserResolver.GetUserId();
                } 
            }
            return base.SaveChanges();
        }
    }
}
