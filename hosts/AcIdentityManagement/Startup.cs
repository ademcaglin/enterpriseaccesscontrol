﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using AccessControl;
using AcIdentityManagement.Infrastructure;

namespace AcIdentityManagement
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
        }
        
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole();
            app.UseDeveloperExceptionPage();
            app.UseAcWeb(new AcWebOptions()
            {
                OpenIdConnectOptions = new OpenIdConnectOptions()
                {
                    AuthenticationScheme = "oidc",
                    SignInScheme = "Cookies",
                    Authority = "http://localhost:1941",
                    RequireHttpsMetadata = false,
                    ClientId = "mvc.hybrid",
                    ClientSecret = "secret",
                    ResponseType = "code id_token",
                    Scope = { "api1", "offline_access" },
                    SaveTokens = true,
                }
            });
            app.UseStaticFiles();
            app.UseAcTransformation();
            //app.Protect("/scopes/management", AcScopes.Management);
            //app.Protect("/scopes/administration", AcScopes.Administration);
            app.UseMvcWithDefaultRoute();
        }
    }
}
