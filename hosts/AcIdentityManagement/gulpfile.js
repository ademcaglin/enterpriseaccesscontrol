﻿var gulp = require('gulp');

gulp.task('default', function () {
    // place code for your default task here
});

var paths = {};
paths.webroot = "wwwroot/";
paths.libSrc = "./ClientApp/assets/";
paths.lib = paths.webroot + "lib/";

gulp.task("copy-lib", function () {
    return gulp.src(paths.libSrc + '/**/*.*', { base: paths.libSrc + '/' })
         .pipe(gulp.dest(paths.lib));
});