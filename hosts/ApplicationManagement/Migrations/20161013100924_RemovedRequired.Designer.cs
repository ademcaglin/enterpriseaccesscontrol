﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using EnterpriseAccessControl.EF.Data;

namespace ApplicationManagement.Migrations
{
    [DbContext(typeof(EacDbContext))]
    [Migration("20161013100924_RemovedRequired")]
    partial class RemovedRequired
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("EnterpriseAccessControl.EF.Core.Applications.Application", b =>
                {
                    b.Property<string>("Id");

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("CreatedBy")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 50);

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 500);

                    b.Property<int>("Status");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 200);

                    b.HasKey("Id");

                    b.ToTable("Applications");
                });

            modelBuilder.Entity("EnterpriseAccessControl.EF.Core.Modules.Module", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ApplicationId");

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("CreatedBy")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 50);

                    b.Property<string>("Description")
                        .HasAnnotation("MaxLength", 500);

                    b.Property<bool>("IsActive");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 200);

                    b.Property<int?>("ResourceTypeId");

                    b.Property<string>("Title")
                        .HasAnnotation("MaxLength", 200);

                    b.HasKey("Id");

                    b.HasIndex("ApplicationId");

                    b.HasIndex("ResourceTypeId");

                    b.ToTable("Modules");
                });

            modelBuilder.Entity("EnterpriseAccessControl.EF.Core.Permissions.Permission", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("CreatedBy")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 50);

                    b.Property<string>("Description")
                        .HasAnnotation("MaxLength", 500);

                    b.Property<bool>("IsActive");

                    b.Property<int?>("ModuleId");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 200);

                    b.Property<string>("Title")
                        .HasAnnotation("MaxLength", 200);

                    b.Property<int?>("TypeId");

                    b.HasKey("Id");

                    b.HasIndex("ModuleId");

                    b.HasIndex("TypeId");

                    b.ToTable("Permissions");
                });

            modelBuilder.Entity("EnterpriseAccessControl.EF.Core.PermissionTypes.PermissionType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("CreatedBy")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 50);

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 500);

                    b.Property<int?>("ModuleId");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 200);

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 200);

                    b.HasKey("Id");

                    b.HasIndex("ModuleId");

                    b.ToTable("PermissionTypes");
                });

            modelBuilder.Entity("EnterpriseAccessControl.EF.Core.Resources.Resource", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("CreatedBy")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 50);

                    b.Property<string>("FullTitle")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 500);

                    b.Property<int>("Status");

                    b.Property<int?>("TypeId");

                    b.Property<string>("Value")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 200);

                    b.Property<string>("ValueTitle")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 200);

                    b.HasKey("Id");

                    b.HasIndex("TypeId");

                    b.ToTable("Resources");
                });

            modelBuilder.Entity("EnterpriseAccessControl.EF.Core.ResourceTypes.ResourceType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("CreatedBy")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 50);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 200);

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 200);

                    b.HasKey("Id");

                    b.ToTable("ResourceTypes");
                });

            modelBuilder.Entity("EnterpriseAccessControl.EF.Core.Roles.Role", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("CreatedBy")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 50);

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 500);

                    b.Property<int?>("ModuleId");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 200);

                    b.Property<int>("Status");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 200);

                    b.HasKey("Id");

                    b.HasIndex("ModuleId");

                    b.ToTable("Roles");
                });

            modelBuilder.Entity("EnterpriseAccessControl.EF.Core.Roles.RolePermission", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("CreatedBy")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 50);

                    b.Property<int?>("PermissionId");

                    b.Property<int?>("RoleId");

                    b.HasKey("Id");

                    b.HasIndex("PermissionId");

                    b.HasIndex("RoleId");

                    b.ToTable("RolePermission");
                });

            modelBuilder.Entity("EnterpriseAccessControl.EF.Core.Users.User", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("CreatedBy")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 50);

                    b.Property<string>("Email");

                    b.Property<bool>("IsActive");

                    b.Property<string>("PhoneNumber");

                    b.Property<string>("UserName");

                    b.HasKey("Id");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("EnterpriseAccessControl.EF.Core.Users.UserRole", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("CreatedBy")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 50);

                    b.Property<int?>("ResourceId");

                    b.Property<int?>("RoleId");

                    b.Property<int>("Status");

                    b.Property<long?>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("ResourceId");

                    b.HasIndex("RoleId");

                    b.HasIndex("UserId");

                    b.ToTable("UserRole");
                });

            modelBuilder.Entity("EnterpriseAccessControl.EF.Core.Modules.Module", b =>
                {
                    b.HasOne("EnterpriseAccessControl.EF.Core.Applications.Application", "Application")
                        .WithMany("Modules")
                        .HasForeignKey("ApplicationId");

                    b.HasOne("EnterpriseAccessControl.EF.Core.ResourceTypes.ResourceType", "ResourceType")
                        .WithMany()
                        .HasForeignKey("ResourceTypeId");
                });

            modelBuilder.Entity("EnterpriseAccessControl.EF.Core.Permissions.Permission", b =>
                {
                    b.HasOne("EnterpriseAccessControl.EF.Core.Modules.Module", "Module")
                        .WithMany("Permissions")
                        .HasForeignKey("ModuleId");

                    b.HasOne("EnterpriseAccessControl.EF.Core.PermissionTypes.PermissionType", "Type")
                        .WithMany()
                        .HasForeignKey("TypeId");
                });

            modelBuilder.Entity("EnterpriseAccessControl.EF.Core.PermissionTypes.PermissionType", b =>
                {
                    b.HasOne("EnterpriseAccessControl.EF.Core.Modules.Module", "Module")
                        .WithMany()
                        .HasForeignKey("ModuleId");
                });

            modelBuilder.Entity("EnterpriseAccessControl.EF.Core.Resources.Resource", b =>
                {
                    b.HasOne("EnterpriseAccessControl.EF.Core.ResourceTypes.ResourceType", "Type")
                        .WithMany()
                        .HasForeignKey("TypeId");
                });

            modelBuilder.Entity("EnterpriseAccessControl.EF.Core.Roles.Role", b =>
                {
                    b.HasOne("EnterpriseAccessControl.EF.Core.Modules.Module", "Module")
                        .WithMany("Roles")
                        .HasForeignKey("ModuleId");
                });

            modelBuilder.Entity("EnterpriseAccessControl.EF.Core.Roles.RolePermission", b =>
                {
                    b.HasOne("EnterpriseAccessControl.EF.Core.Permissions.Permission", "Permission")
                        .WithMany()
                        .HasForeignKey("PermissionId");

                    b.HasOne("EnterpriseAccessControl.EF.Core.Roles.Role", "Role")
                        .WithMany("Permissions")
                        .HasForeignKey("RoleId");
                });

            modelBuilder.Entity("EnterpriseAccessControl.EF.Core.Users.UserRole", b =>
                {
                    b.HasOne("EnterpriseAccessControl.EF.Core.Resources.Resource", "Resource")
                        .WithMany()
                        .HasForeignKey("ResourceId");

                    b.HasOne("EnterpriseAccessControl.EF.Core.Roles.Role", "Role")
                        .WithMany()
                        .HasForeignKey("RoleId");

                    b.HasOne("EnterpriseAccessControl.EF.Core.Users.User", "User")
                        .WithMany("Roles")
                        .HasForeignKey("UserId");
                });
        }
    }
}
