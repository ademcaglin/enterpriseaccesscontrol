﻿using ApplicationManagement.Constants;
using EnterpriseAccessControl.Attributes;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationManagement.Areas.Administration.Controllers
{
    [Area(EacModules.Administration)]
    public abstract class BaseController : Controller
    {

    }
}
