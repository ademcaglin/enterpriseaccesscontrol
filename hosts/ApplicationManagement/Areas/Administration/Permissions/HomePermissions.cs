﻿using ApplicationManagement.Constants;
using EnterpriseAccessControl.Attributes;

namespace ApplicationManagement.Areas.Administration.Permissions
{
    [EacPermissions(EacModules.Administration)]
    public class HomePermissions
    {
        private const string Category = "HOME";

        private const string Prefix = EacModules.Administration + "-" + Category + "-";

        public const string Index = Prefix + "INDEX";

        public const string View = Prefix + "VIEW";

        //public const string Disable = Prefix + "DISABLE";
    }
}
