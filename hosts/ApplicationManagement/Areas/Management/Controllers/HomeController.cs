﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ApplicationManagement.Constants;
using EnterpriseAccessControl.Attributes;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace ApplicationManagement.Areas.Management.Controllers
{
    [Area(EacModules.Management)]
    public class HomeController : Controller
    {
        //[Authorize(ActiveAuthenticationSchemes = "oidc")]
        public IActionResult Index()
        {
            return View();
        }
    }
}
