﻿using ApplicationManagement.Constants;
using EnterpriseAccessControl.Attributes;

namespace ApplicationManagement.Areas.Management
{
    [EacPermissions(EacModules.Management)]
    public class Permissions
    {
        private const string Category = "HOME";

        private const string Prefix = EacModules.Administration + "-" + Category + "-";

        public const string Index = Prefix + "INDEX";
    }
}
