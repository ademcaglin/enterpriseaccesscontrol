﻿/// <binding BeforeBuild='Run - Development' />
module.exports = [
  {
      entry: {
          core: './node_modules/core-js/client/shim.min.js',
          zone: './node_modules/zone.js/dist/zone.js',
          reflect: './node_modules/reflect-metadata/Reflect.js',
          system: './node_modules/systemjs/dist/system.src.js'
      },
      output: {
          filename: './wwwroot/lib/js/[name].js'
      },
      target: 'web',
      node: {
          fs: "empty"
      }
  },
  {
      entry: {
          app: './ClientApp/modules/management/app/main.ts',
          config : './ClientApp/modules/management/system.config.js'
      },
      output: {
          filename: './wwwroot/apps/management/[name].js'
      },
      devtool: 'source-map',
      resolve: {
          extensions: ['', '.webpack.js', '.web.js', '.ts', '.js']
      },
      module: {
          loaders: [
            { test: /\.ts$/, loader: 'ts-loader' }
          ]
      }
  }];