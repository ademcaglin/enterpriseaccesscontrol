﻿/*import { Injectable }       from '@angular/core';
import { CanActivate }      from '@angular/router';
import {
    Http,
    Response,
    Headers,
    Request,
    RequestMethod,
    RequestOptions
} from '@angular/http';
import { ActivatedRoute, Router }   from '@angular/router';
import { Observable }       from 'rxjs/Observable';
import 'rxjs/add/operator/share';
import 'rxjs/rx';


export interface AuthData {
    accessToken: string;
    expiry: string;
    moduleName: string;
    resourceValue: string;
    issueDate: string;
}

@Injectable()
export class AuthService {
    constructor(private http: Http) { }

    private _currentAuthData: AuthData;

    get currentAuthData(): AuthData {
        return this._currentAuthData;
    }

    getRemoteToken(): Observable<string> {
        return this.http
            .get('/access_token')
            .map((r: Response) => r.json().data as string);
    }
    
    get(path: string): Observable<Response> {
        return this.sendHttpRequest(new RequestOptions({
            method: RequestMethod.Get,
            url: this._constructApiPath() + path
        }));
    }

    post(path: string, data: any): Observable<Response> {
        return this.sendHttpRequest(new RequestOptions({
            method: RequestMethod.Post,
            url: this._constructApiPath() + path,
            body: data
        }));
    }

    // Construct and send Http request
    sendHttpRequest(requestOptions: RequestOptions): Observable<Response> {

        let baseRequestOptions: RequestOptions;
        let baseHeaders: { [key: string]: string; } = null;//this._options.globalOptions.headers;

        // Merge auth headers to request if set
        if (this._currentAuthData != null) {
            (<any>Object).assign(baseHeaders, {
                'access-token': this._currentAuthData.accessToken,
                'expiry': this._currentAuthData.expiry,
                //'uid': this._currentAuthData.uid
            });
        }

        baseRequestOptions = new RequestOptions({
            headers: new Headers(baseHeaders)
        });

        // Merge standard and custom RequestOptions
        baseRequestOptions = baseRequestOptions.merge(requestOptions);

        let response = this.http.request(new Request(baseRequestOptions)).share();

        this._handleResponse(response);

        return response;
    }

    // Check if response is complete and newer, then update storage
    private _handleResponse(response: Observable<Response>) {
        response.subscribe(res => {
            this._parseAuthHeadersFromResponse(<any>res);
        }, error => {
            this._parseAuthHeadersFromResponse(<any>error);
            console.log('Session Service: Error Fetching Response');
        });
    }

    private _parseAuthHeadersFromResponse(data: any) {
        let headers = data.headers;

        let authData: AuthData = {
            accessToken: headers.get('access-token'),
            expiry: headers.get('expiry'),
            //uid: headers.get('uid')
        };

        this._setAuthData(authData);
    }

    // Try to get auth data from storage.
    private _getAuthDataFromStorage() {

        let authData: AuthData = {
            accessToken: localStorage.getItem('accessToken'),
            expiry: localStorage.getItem('expiry'),
            //uid: localStorage.getItem('uid')
        };

        if (this._checkIfComplete(authData))
            this._currentAuthData = authData;
    }

    // Write auth data to storage
    private _setAuthData(authData: AuthData) {

        if (this._checkIfComplete(authData) && this._checkIfNewer(authData)) {

            this._currentAuthData = authData;

            localStorage.setItem('accessToken', authData.accessToken);
            localStorage.setItem('expiry', authData.expiry);
            localStorage.setItem('uid', authData.uid);
            
        }
    }

    // Check if auth data complete
    private _checkIfComplete(authData: AuthData): boolean {
        if (
            authData.accessToken != null &&
            authData.expiry != null &&
            authData.uid != null
        ) {
            return true;
        } else {
            return false;
        }
    }

    // Check if response token is newer
    private _checkIfNewer(authData: AuthData): boolean {
        if (this._currentAuthData != null)
            return authData.expiry >= this._currentAuthData.expiry;
        else
            return true;
    }

    // Try to load user config from storage
    private _tryLoadAuthData() {

        this._getAuthDataFromStorage();

        //if (this._currentAuthData != null)
        //    this.validateToken();
    }

    private _constructApiPath(): string {
         return  '/';
    }
}*/