﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ApplicationManagement.Constants;
using EnterpriseAccessControl.Extensions;
using EnterpriseAccessControl.Core;
using System.Reflection;
using EnterpriseAccessControl;
using EnterpriseAccessControl.EF.Services;
using EnterpriseAccessControl.EF.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Security.Claims;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication;
using IdentityModel.Client;
using Newtonsoft.Json;
using Microsoft.AspNetCore.DataProtection;


namespace ApplicationManagement
{
    public class Startup
    {
        private const string ApplicationName = "ENTERPRISEACCESSCONTROL";
        private const string ConStr = @"Server=(localdb)\mssqllocaldb;Database=ApplicationManagement;Trusted_Connection=True;";
        private const string IdpEndpoint = "http://localhost:1941";

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddScoped<IEacUserService, EacUserService>();
            services.AddSingleton<IEacApplicationService, EacApplicationService>();
            services.Configure<EacOptions>(app =>
            {
                app.ApplicationName = ApplicationName;
                app.ApplicationAssembly = GetType().GetTypeInfo().Assembly;
                app.AddModule(EacModules.Administration);
                app.AddModule(EacModules.Management, ResourceTypes.Application);
                app.FullMvcOptions = new FullMvcOptions()
                {
                    OpenIdConnectOptions = new OpenIdConnectOptions
                    {
                        AuthenticationScheme = "oidc",
                        SignInScheme = "Cookies",
                        Authority = IdpEndpoint,
                        RequireHttpsMetadata = false,
                        ClientId = "mvc.hybrid",
                        ClientSecret = "secret",
                        ResponseType = "code id_token",
                        Scope = { "api1", "offline_access" },
                        SaveTokens = true,
                    }
                };
            });
            services.AddEac();
            services.AddOptions();
            services.AddDbContext<EacDbContext>(options =>
                 options.UseSqlServer(ConStr, b => b.MigrationsAssembly("ApplicationManagement")));
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole();
            app.UseDeveloperExceptionPage();
            app.UseIdentityServerAuthentication(new IdentityServerAuthenticationOptions
            {
                Authority = "http://localhost:1941",
                ScopeName = "api1",
                RequireHttpsMetadata = false
            });




            CreateSampleApplicationAndResources();
            app.UseEac();
            CreateSampleRoleAndUsers();
            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute("Management",
                    "{area}/{controller}/{action}/{id?}",
                    new { controller = "Home", action = "Index" },
                    new { area = EacModules.Management });
                routes.MapRoute("Administration",
                    "{area}/{controller}/{action}/{id?}",
                    new { controller = "Home", action = "Index" },
                    new { area = EacModules.Administration });
                routes.MapRoute("Default",
                    "{controller=Home}/{action=Index}/{id?}");
            });

        }

        private void CreateSampleApplicationAndResources()
        {
            var optionsBuilder = new DbContextOptionsBuilder<EacDbContext>();
            optionsBuilder.UseSqlServer(ConStr);
            using (var context = new EacDbContext(null, optionsBuilder.Options))
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();

                var app = new EnterpriseAccessControl.EF.Core.Applications.Application()
                {
                    Id = ApplicationName,
                    CreatedAt = DateTime.Now,
                    CreatedBy = "SYSTEM",
                    Description = "APP MANAGEMENT",
                    Status = EnterpriseAccessControl.EF.Core.Applications.ApplicationStatus.Enabled,
                    Title = "APPLICATION MANAGEMENT"
                };
                context.Applications.Add(app);
                var appResourceType = new EnterpriseAccessControl.EF.Core.ResourceTypes.ResourceType()
                {
                    CreatedBy = "SYSTEM",
                    Name = ResourceTypes.Application,
                    Title = "Application"
                };
                context.ResourceTypes.Add(appResourceType);

                var resource1 = new EnterpriseAccessControl.EF.Core.Resources.Resource()
                {
                    CreatedBy = "SYSTEM",
                    Status = EnterpriseAccessControl.EF.Core.Resources.ResourceStatus.Enabled,
                    Type = appResourceType,
                    Value = ApplicationName,
                    ValueTitle = ApplicationName,
                    FullTitle = ApplicationName
                };
                context.Resources.Add(resource1);

                context.SaveChanges();
            }
        }

        private void CreateSampleRoleAndUsers()
        {
            var optionsBuilder = new DbContextOptionsBuilder<EacDbContext>();
            optionsBuilder.UseSqlServer(ConStr);
            using (var context = new EacDbContext(null, optionsBuilder.Options))
            {
                var app = context.Applications
                    .Include(x => x.Modules).ThenInclude(y => y.Permissions)
                    .SingleOrDefault(x => x.Id == ApplicationName);
                var module = app.Modules.SingleOrDefault(x => x.Name == EacModules.Administration);
                var managementModule = app.Modules.SingleOrDefault(x => x.Name == EacModules.Management);
                var fullAdmin = new EnterpriseAccessControl.EF.Core.Roles.Role()
                {
                    CreatedBy = "SYSTEM",
                    Description = "FULL ADMIN",
                    Module = module,
                    Name = "FULL_ADMIN",
                    Status = EnterpriseAccessControl.EF.Core.Roles.RoleStatus.Enabled,
                    Title = "FULL ADMIN"
                };

                var managementAdmin = new EnterpriseAccessControl.EF.Core.Roles.Role()
                {
                    CreatedBy = "SYSTEM",
                    Description = "MANAGEMENT ADMIN",
                    Module = managementModule,
                    Name = "MANAGEMENT_ADMIN",
                    Status = EnterpriseAccessControl.EF.Core.Roles.RoleStatus.Enabled,
                    Title = "MANAGEMENT ADMIN",

                };

                foreach (var perm in module.Permissions.Where(x => x.IsActive))
                {
                    fullAdmin.Permissions.Add(new EnterpriseAccessControl.EF.Core.Roles.RolePermission()
                    {
                        CreatedBy = "SYSTEM",
                        Role = fullAdmin,
                        Permission = perm
                    });
                }

                foreach (var perm in managementModule.Permissions.Where(x => x.IsActive))
                {
                    managementAdmin.Permissions.Add(new EnterpriseAccessControl.EF.Core.Roles.RolePermission()
                    {
                        CreatedBy = "SYSTEM",
                        Role = managementAdmin,
                        Permission = perm
                    });
                }

                context.Roles.Add(fullAdmin);
                context.Roles.Add(managementAdmin);

                var user = new EnterpriseAccessControl.EF.Core.Users.User()
                {
                    CreatedBy = "SYSTEM",
                    Email = "a@b.com",
                    IsActive = true,
                    PhoneNumber = "3122031000",
                    UserName = "bob",
                    Id = "88421113"
                };
                user.Roles.Add(new EnterpriseAccessControl.EF.Core.Users.UserRole()
                {
                    Role = fullAdmin,
                    CreatedBy = "SYSTEM",
                    Status = EnterpriseAccessControl.EF.Core.Users.UserRoleStatus.Enabled,
                    User = user
                });
                user.Roles.Add(new EnterpriseAccessControl.EF.Core.Users.UserRole()
                {
                    Role = managementAdmin,
                    CreatedBy = "SYSTEM",
                    Status = EnterpriseAccessControl.EF.Core.Users.UserRoleStatus.Enabled,
                    User = user,
                    Resource = context.Resources.FirstOrDefault(x => x.Id == 1)
                });
                context.Users.Add(user);
                context.SaveChanges();
            }
        }
    }
}
