﻿
using EnterpriseAccessControl.Attributes;

namespace ApplicationManagement.Constants
{
    public class EacModules
    {
        public const string Administration = "ADMINISTRATION";

        public const string Management = "MANAGEMENT";

        //public const string User = "USER";
    }
}
