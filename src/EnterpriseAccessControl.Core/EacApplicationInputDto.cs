﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnterpriseAccessControl.Core
{
    public class EacApplicationInputDto
    {
        public string Name { get; set; }

        public IList<EacModuleInputDto> Modules { get; set; }

        public EacApplicationInputDto()
        {
            Modules = new List<EacModuleInputDto>();
        }
    }
}
