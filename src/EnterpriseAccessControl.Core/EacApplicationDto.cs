﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnterpriseAccessControl.Core
{
    public class EacApplicationDto
    {
        public string Name { get; set; }

        public string Title { get; set; }

        public IList<EacModuleDto> Modules { get; set; }

        public EacApplicationDto()
        {
            Modules = new List<EacModuleDto>();
        }
    }
}
