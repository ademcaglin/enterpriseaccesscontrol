﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnterpriseAccessControl.Core
{
    public interface IEacUserResolver
    {
        Task<EacUserDto> GetUser();
    }
}
