﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnterpriseAccessControl.Core
{
    public class EacUserInfoInput
    {
        public string ApplicationName { get; set; }

        //public string ModuleName { get; set; }

        //public string ResourceValue { get; set; }

        public string UserId { get; set; }
    }
}
