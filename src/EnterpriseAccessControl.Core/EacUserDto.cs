﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnterpriseAccessControl.Core
{
    public class EacUserDto
    {
        public string UserId { get; set; }

        public string UserName { get; set; }

        public IList<EacModuleDto> Modules { get; set; } = new List<EacModuleDto>();

        public bool Success { get; set; }

        public EacModuleDto CurrentModule { get; set; }
    }
}
