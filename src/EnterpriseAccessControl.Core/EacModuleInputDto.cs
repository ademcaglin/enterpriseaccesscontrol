﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnterpriseAccessControl.Core
{
    public class EacModuleInputDto
    {
        public string Name { get; set; }

        public string ResourceTypeName { get; set; }

        public IList<EacPermissionInputDto> Permissions { get; set; }

        public EacModuleInputDto()
        {
            Permissions = new List<EacPermissionInputDto>();
        }
    }
}
