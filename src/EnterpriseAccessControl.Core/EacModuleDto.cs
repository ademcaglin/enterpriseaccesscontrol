﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnterpriseAccessControl.Core
{
    public class EacModuleDto
    {
        public string Name { get; set; }

        public string Title { get; set; }

        public string ResourceName { get; set; }

        public string ResourceValue { get; set; }

        public string ResourceTitle { get; set; }

        public IEnumerable<string> Permissions { get; set; }
    }
}
