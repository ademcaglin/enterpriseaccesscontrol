﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnterpriseAccessControl.Core
{
    public class EacPermissionInputDto
    {
        public string Name { get; set; }

        public string PermissionType { get; set; }
    }
}
