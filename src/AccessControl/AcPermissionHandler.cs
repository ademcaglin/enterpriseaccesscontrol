﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using System.Threading.Tasks;

namespace AccessControl
{
    internal class AcPermissionHandler : AuthorizationHandler<AcPermissionRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, AcPermissionRequirement requirement)
        {
            if (context.User.Identity.IsAuthenticated)
            {
                var hasPermission = context.User.HasClaim(x =>
                   x.Type == "" && x.Value == requirement.Name);
                if (hasPermission)
                {
                    context.Succeed(requirement);
                }
            }
            return Task.FromResult(0);
        }
    }
}
