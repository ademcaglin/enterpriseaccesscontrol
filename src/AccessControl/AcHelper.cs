﻿using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace AccessControl
{
    internal class AcHelper
    {
        public static AcPermission GetPermission(ActionDescriptor action)
        {
            var descriptor = action as ControllerActionDescriptor;
            var authorizeAttr = descriptor.MethodInfo.GetCustomAttribute<AcAuthorizeAttribute>();
            if (authorizeAttr != null)
            {
                var perm = new AcPermission();
                perm.Name = descriptor.MethodInfo.Name.ToUpperInvariant();
                perm.PermissionType = descriptor.ControllerName.ToUpperInvariant();
                perm.Scope = authorizeAttr.Scope;
                return perm;
            }
            return null;
            //var actionName = descriptor.MethodInfo.Name.ToUpperInvariant();
            //var ctrlName = descriptor.ControllerName.ToUpperInvariant();
            //var permName = scopeName + "-" + ctrlName + "-" + actionName;
            //return permName;
        }
    }
}
