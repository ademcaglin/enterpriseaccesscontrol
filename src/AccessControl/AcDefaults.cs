﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccessControl
{
    public class AcDefaults
    {
        public const string AcIdentity = "ACIDENTITY";

        public const string AcResourceValue = "ACRESOURCEVALUE";

        public const string AcModuleName = "ACMODULENAME";

        public const string CookieAuthenticationScheme = "Cookies";
    }
}
