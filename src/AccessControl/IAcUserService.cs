﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AccessControl.Core
{
    public interface IAcUserService
    {
        Task<AcUserResult> GetUser(string applicationId, string userId);
    }
}
