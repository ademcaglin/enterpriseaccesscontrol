﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AccessControl
{
    public interface IAcApplicationService
    {
        void SyncClaims(string applicationId, IEnumerable<AcPermission> permissions);
    }
}
