﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccessControl
{
    public class AcPermission
    {
        public string Name { get; set; }

        public string PermissionType { get; set; }

        public string Scope { get; set; }

        public string GetFullName()
        {
            return Scope + "-" + PermissionType + "-" + Name;
        }
    }
}
