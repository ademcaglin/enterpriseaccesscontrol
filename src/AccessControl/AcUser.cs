﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AccessControl
{
    public class AcUser
    {
        public ClaimsIdentity Identity { get; set; }

        public IList<AcScope> Scopes { get; set; } = new List<AcScope>();
    }
}
