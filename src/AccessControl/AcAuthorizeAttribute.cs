﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccessControl
{
    public class AcAuthorizeAttribute : Attribute
    {
        public string Scope { get; set; }

        public string Resource { get; set; }
    }
}
