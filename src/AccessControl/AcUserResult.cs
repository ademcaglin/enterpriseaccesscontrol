﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccessControl
{
    public class AcUserResult
    {
        public bool Success { get; set; }

        public string ErrorCode { get; set; }

        public IList<AcScope> Scopes { get; set; } = new List<AcScope>();
    }
}
