﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;

namespace AccessControl
{
    public class AcOptions
    {
        public string ApplicationName { get; set; }

        public Assembly Assembly { get; set; }

        public string SchemeName { get; set; } = "Bearer";
    }

    public class AcWebOptions
    {
        public string TokenEndpoint { get; set; } = "/ac_token";

        public OpenIdConnectOptions OpenIdConnectOptions { get; set; }
    }
}
