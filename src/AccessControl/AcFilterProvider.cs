﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Reflection;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.Extensions.Options;

namespace AccessControl
{
    internal class AcFilterProvider : IFilterProvider
    {
        private readonly IOptions<AcOptions> _options;
        public AcFilterProvider(IOptions<AcOptions> options)
        {
            _options = options;
        }
        public int Order
        {
            get
            {
                return 0;
            }
        }

        public void OnProvidersExecuted(FilterProviderContext context)
        {
        }

        public void OnProvidersExecuting(FilterProviderContext context)
        {
            var ctrl = context.ActionContext.ActionDescriptor as ControllerActionDescriptor;
            var authorizeAttr = ctrl.MethodInfo.GetCustomAttribute<AcAuthorizeAttribute>();
            if (authorizeAttr == null)
            {
                return;
            }
 
            var user = context.ActionContext.HttpContext.User;
            var perm = AcHelper.GetPermission(context.ActionContext.ActionDescriptor);
            var policy = new AuthorizationPolicyBuilder()
                .AddAuthenticationSchemes(_options.Value.SchemeName)
                .AddRequirements(new AcPermissionRequirement(perm.GetFullName()));
            var filter = new AuthorizeFilter(policy.Build());
            context.Results.Insert(0, new FilterItem(new FilterDescriptor(filter, FilterScope.Action), filter));

            
        }
    }
}
