﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Options;

namespace AccessControl
{
    public static class EacServiceCollectionExtension
    {
        public static void AddEac(this IServiceCollection services)
        {
            services.AddSingleton<IAuthorizationHandler, AcPermissionHandler>();
            services.TryAddEnumerable(ServiceDescriptor.Singleton<IFilterProvider, AcFilterProvider>());
        }
    }
}
