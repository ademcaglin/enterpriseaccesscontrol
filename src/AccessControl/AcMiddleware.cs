﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace AccessControl
{
    public class AcMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IOptions<AcOptions> _options;

        public AcMiddleware(RequestDelegate next, IAcApplicationService appService,
            IOptions<AcOptions> options, IActionDescriptorCollectionProvider actionDescriptorCollectionProvider)
        {
            _next = next;
            _options = options;
            var actions = GetPermissions(options.Value, actionDescriptorCollectionProvider);
            appService.SyncClaims(options.Value.ApplicationName, actions);
        }

        public async Task Invoke(HttpContext context)
        {
            await _next.Invoke(context);
        }

        private IEnumerable<AcPermission> GetPermissions(AcOptions options, IActionDescriptorCollectionProvider actionDescriptorCollectionProvider)
        {
            var list = new List<AcPermission>();
            var customPermissionTypes = options.Assembly.GetTypes().Where(x =>
                x.GetTypeInfo().GetCustomAttribute<AcPermissionsAttribute>() != null);

            foreach (var type in customPermissionTypes)
            {
                var attr = type.GetTypeInfo().GetCustomAttribute<AcPermissionsAttribute>();
                var fields = type.GetFields(BindingFlags.Static | BindingFlags.Public).Where(f => f.IsLiteral);
                foreach (var field in fields)
                {
                    var perm = new AcPermission();
                    perm.Name = field.GetRawConstantValue().ToString();
                    perm.PermissionType = attr.PermissionType;
                    perm.Scope = attr.Scope;
                    list.Add(perm);
                }
            }

            var ctrlActions = actionDescriptorCollectionProvider.ActionDescriptors.Items.ToList();
            foreach (var action in ctrlActions)
            {
                var perm = AcHelper.GetPermission(action);
                if (perm != null)
                    list.Add(perm);
            }
            return list;

        }

    }
}
