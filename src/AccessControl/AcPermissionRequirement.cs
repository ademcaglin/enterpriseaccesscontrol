﻿using Microsoft.AspNetCore.Authorization;

namespace AccessControl
{
    internal class AcPermissionRequirement : IAuthorizationRequirement
    {
        public AcPermissionRequirement(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
    }
}
