﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccessControl
{
    public class AcPermissionsAttribute : Attribute
    {
        public string Scope { get; set; }

        public string PermissionType { get; set; }
    }
}
