﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;
using IdentityModel.Client;

namespace AccessControl
{
    public static class EacApplicationBuilderExtension
    {
        public static void UseAcTransformation(this IApplicationBuilder app)
        {
            app.UseMiddleware<AcMiddleware>();
        }

        public static void UseAcWeb(this IApplicationBuilder app, AcWebOptions options)
        {
            var oidcOptions = options.OpenIdConnectOptions;
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationScheme = AcDefaults.CookieAuthenticationScheme
            });
            app.UseOpenIdConnectAuthentication(oidcOptions);
            app.Map(options.TokenEndpoint, tokenBuilder =>
            {
                tokenBuilder.Run(async (context) =>
                {
                    TokenResponse response = null;
                    var refreshToken = await context.Authentication.GetTokenAsync("refresh_token", "Cookies");
                    var tokenClient = new TokenClient(oidcOptions.Authority + "/connect/token", oidcOptions.ClientId, oidcOptions.ClientSecret);
                    response = await tokenClient.RequestRefreshTokenAsync(refreshToken);
                    var accessToken = response.AccessToken;
                    var expiresIn = response.ExpiresIn;
                    context.Response.ContentType = "application/json";
                    context.Response.StatusCode = 200;
                    var result = new
                    {
                        accessToken = accessToken,
                        expiresIn = expiresIn,
                        issueDate = DateTime.UtcNow
                    };
                    await context.Response.WriteAsync(JsonConvert.SerializeObject(result));
                });
            });
        }

        public static void Protect(this IApplicationBuilder app, string startsWith, string scope = "")
        {
            app.UseWhen(x => x.Request.Path.StartsWithSegments(startsWith), builder =>
             {
                 builder.Use(async (context, next) =>
                 {
                     var auth = await context.Authentication.AuthenticateAsync(AcDefaults.CookieAuthenticationScheme);
                     if (!auth.Identity.IsAuthenticated)
                     {
                         await context.Authentication.ChallengeAsync(AcDefaults.CookieAuthenticationScheme);
                         return;
                     }
                     if (!string.IsNullOrEmpty(scope))
                     {

                     }
                     await Task.CompletedTask;
                 });
             });
        }
    }
}
