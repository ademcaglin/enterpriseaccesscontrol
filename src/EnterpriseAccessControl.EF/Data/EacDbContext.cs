﻿using EnterpriseAccessControl.EF.Core.Applications;
using EnterpriseAccessControl.EF.Core.Permissions;
using EnterpriseAccessControl.EF.Core.Resources;
using EnterpriseAccessControl.EF.Core.ResourceTypes;
using EnterpriseAccessControl.EF.Core.Modules;
using EnterpriseAccessControl.EF.Core.Roles;
using EnterpriseAccessControl.EF.Core.Users;
using Microsoft.EntityFrameworkCore;
using EnterpriseAccessControl.Core;
using EnterpriseAccessControl.EF.Core;
using System;
using System.Linq;
using EnterpriseAccessControl.EF.Core.PermissionTypes;

namespace EnterpriseAccessControl.EF.Data
{
    public class EacDbContext : DbContext
    {
        private readonly IEacUserResolver _eacUserResolver;

        public EacDbContext()
        {
        }

        public EacDbContext(IEacUserResolver eacUserResolver, DbContextOptions options)
            : base(options)
        {
            _eacUserResolver = eacUserResolver;
        }

        public DbSet<Application> Applications { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<Permission> Permissions { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Module> Modules { get; set; }

        public DbSet<Resource> Resources { get; set; }

        public DbSet<ResourceType> ResourceTypes { get; set; }

        public DbSet<PermissionType> PermissionTypes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Module>()
                   .HasOne(x => x.Application)
                   .WithMany(x => x.Modules);

            modelBuilder.Entity<Permission>()
                   .HasOne(x => x.Module)
                   .WithMany(x => x.Permissions);

            modelBuilder.Entity<Resource>()
                   .HasOne(x => x.Type);

            modelBuilder.Entity<Permission>()
                   .HasOne(x => x.Type);

            modelBuilder.Entity<Role>()
                   .HasOne(x => x.Module)
                   .WithMany(x => x.Roles);

            modelBuilder.Entity<RolePermission>()
                  .HasOne(x => x.Role)
                  .WithMany(x => x.Permissions);

            modelBuilder.Entity<UserRole>()
                   .HasOne(x => x.User)
                   .WithMany(x => x.Roles);

            modelBuilder.Entity<UserRole>()
                  .HasOne(x => x.Role);
        }

        public override int SaveChanges()
        {
            
            var addedAuditedEntities = ChangeTracker.Entries<ICreateAuditedEntity>()
               .Where(p => p.State == EntityState.Added)
               .Select(p => p.Entity);

            var now = DateTime.Now;

            foreach (var added in addedAuditedEntities)
            {
                added.CreatedAt = now;
                if (_eacUserResolver != null)
                {
                    var user = _eacUserResolver.GetUser().Result;
                    added.CreatedBy = user.UserId;
                } 
            }
            return base.SaveChanges();
        }
    }
}
