﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnterpriseAccessControl.EF
{
    public class Check
    {
        public static void True(bool exp)
        {
            if (!exp)
            {
                throw new InvalidOperationException("Expression must be true");
            }
        }
        public static void NotNull(object o)
        {
            if (o == null)
            {
                throw new InvalidOperationException("Object must have a value");
            }
        }
    }
}
