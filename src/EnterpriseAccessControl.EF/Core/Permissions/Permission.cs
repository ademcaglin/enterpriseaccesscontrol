﻿using EnterpriseAccessControl.EF.Core.Modules;
using EnterpriseAccessControl.EF.Core.PermissionTypes;
using System;
using System.ComponentModel.DataAnnotations;

namespace EnterpriseAccessControl.EF.Core.Permissions
{
    public class Permission : CreateAuditedEntity<int>
    {
        [Required]
        [StringLength(200)]
        public string Name { get; set; }

        [StringLength(200)]
        public string Title { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        public bool IsActive { get; set; }

        public virtual PermissionType Type { get; set; }

        public virtual Module Module { get; set; }
    }
}
