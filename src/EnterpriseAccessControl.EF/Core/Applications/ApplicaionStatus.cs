﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EnterpriseAccessControl.EF.Core.Applications
{
    public enum ApplicationStatus
    {
        [Display(Name = "New")]
        New = 0,
        [Display(Name = "Active")]
        Enabled = 1,
        [Display(Name = "Passive")]
        Disabled = 2
    }
}
