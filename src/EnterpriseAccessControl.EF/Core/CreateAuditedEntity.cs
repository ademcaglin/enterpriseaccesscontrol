﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EnterpriseAccessControl.EF.Core
{
    public abstract class CreateAuditedEntity<T> : Entity<T>, ICreateAuditedEntity
    {
        [Required]
        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime CreatedAt { get; set; }
    }

    public interface ICreateAuditedEntity
    {
        string CreatedBy { get; set; }

        DateTime CreatedAt { get; set; }
    }
}
