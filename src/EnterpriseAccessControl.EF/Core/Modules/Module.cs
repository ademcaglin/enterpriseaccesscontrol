﻿using EnterpriseAccessControl.EF.Core.Applications;
using EnterpriseAccessControl.EF.Core.Permissions;
using EnterpriseAccessControl.EF.Core.ResourceTypes;
using EnterpriseAccessControl.EF.Core.Roles;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EnterpriseAccessControl.EF.Core.Modules
{
    public class Module : CreateAuditedEntity<int>
    {
        [Required]
        [StringLength(200)]
        public string Name { get; set; }

        [StringLength(200)]
        public string Title { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        public bool IsActive { get; set; }

        public Application Application { get; set; }

        public ResourceType ResourceType { get; set; }

        public virtual ICollection<Permission> Permissions { get; } = new List<Permission>();

        public virtual ICollection<Role> Roles { get; } = new List<Role>();
    }
}
