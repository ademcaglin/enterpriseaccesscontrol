﻿using EnterpriseAccessControl.EF.Core.Modules;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EnterpriseAccessControl.EF.Core.Roles
{
    public class Role : CreateAuditedEntity<int>
    {
        [Required]
        [StringLength(200)]
        public string Name { get; set; }

        [Required]
        [StringLength(200)]
        public string Title { get; set; }

        [Required]
        [StringLength(500)]
        public string Description { get; set; }

        public RoleStatus Status { get; set; }

        public Module Module { get; set; }

        public ICollection<RolePermission> Permissions { get; set; } = new List<RolePermission>();

        public void Enable()
        {
            Check.True(Status != RoleStatus.Enabled);
            Status = RoleStatus.Enabled;
        }

        public void Disable()
        {
            Check.True(Status == RoleStatus.Enabled);
            Status = RoleStatus.Disabled;
        }

        public bool CanDelete()
        {
            return Status == RoleStatus.New;
        }
        public bool CanEdit()
        {
            return Status == RoleStatus.New;
        }
        public bool CanEnable()
        {
            return Status == RoleStatus.New;
        }
        public bool CanDisable()
        {
            return Status == RoleStatus.Enabled;
        }
    }
}
