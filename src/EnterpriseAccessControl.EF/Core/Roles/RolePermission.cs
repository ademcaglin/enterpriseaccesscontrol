﻿
using EnterpriseAccessControl.EF.Core.Permissions;

namespace EnterpriseAccessControl.EF.Core.Roles
{
    public class RolePermission : CreateAuditedEntity<int>
    {
        public Role Role { get; set; }

        public Permission Permission { get; set; }
    }
}
