﻿using System.ComponentModel.DataAnnotations;

namespace EnterpriseAccessControl.EF.Core.Roles
{
    public enum RoleStatus
    {
        [Display(Name = "New")]
        New = 0,
        [Display(Name = "Active")]
        Enabled = 1,
        [Display(Name = "Passive")]
        Disabled =2
    }
}
