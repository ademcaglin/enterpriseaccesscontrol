﻿using EnterpriseAccessControl.Core;
using EnterpriseAccessControl.EF.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnterpriseAccessControl.EF.Services
{
    public class EacUserService : IEacUserService
    {
        private readonly EacDbContext _dbContext;
        public EacUserService(EacDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<EacUserDto> GetUserInfo(EacUserInfoInput input)
        {
            var userDto = new EacUserDto();
            var user = await _dbContext.Users
                .Include(x => x.Roles).ThenInclude(y => y.Role).ThenInclude(z => z.Module).ThenInclude(t => t.Application)
                .Include(x => x.Roles).ThenInclude(y => y.Role).ThenInclude(z => z.Module).ThenInclude(t => t.ResourceType)
                .Include(x => x.Roles).ThenInclude(y => y.Role).ThenInclude(z => z.Module).ThenInclude(t => t.Permissions).ThenInclude(u => u.Type)
                .Include(x => x.Roles).ThenInclude(y => y.Role).ThenInclude(z => z.Permissions)
                .Include(x => x.Roles).ThenInclude(y => y.Resource)
                .SingleOrDefaultAsync(x => x.Id == input.UserId);

            userDto.UserId = user.Id.ToString();
            userDto.UserName = user.UserName;

            var userRoles = user.Roles.Where(x =>
                x.Status == Core.Users.UserRoleStatus.Enabled &&
                x.Role.Status == Core.Roles.RoleStatus.Enabled &&
                x.Role.Module.Application.Id == input.ApplicationName &&
                x.Role.Module.IsActive)
               .ToList();
            foreach(var userRole in userRoles)
            {
                if (!userDto.Modules.Any(x => x.Name == userRole.Role.Module.Name && x.ResourceValue == userRole.Resource?.Value))
                {
                    var mDto = new EacModuleDto();
                    mDto.Name = userRole.Role.Module.Name;
                    mDto.Title = userRole.Role.Module.Name;
                    mDto.ResourceTitle = userRole.Role.Module.ResourceType?.Title;
                    mDto.ResourceValue = userRole.Resource?.Value;
                    mDto.ResourceName = userRole.Resource?.Type.Name;
                    mDto.Permissions = userRoles.Where(x => mDto.Name == x.Role.Module.Name)
                        .SelectMany(x => x.Role.Permissions)
                        .Select(y => y.Permission.Name);
                    userDto.Modules.Add(mDto);
                }
            }
            
            userDto.Success = true;
            return userDto;
        }
    }
}
