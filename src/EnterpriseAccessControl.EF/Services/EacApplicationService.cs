﻿using EnterpriseAccessControl.Core;
using EnterpriseAccessControl.EF.Core.Modules;
using EnterpriseAccessControl.EF.Core.Permissions;
using EnterpriseAccessControl.EF.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnterpriseAccessControl.EF.Services
{
    public class EacApplicationService : IEacApplicationService
    {
        private readonly DbContextOptions<EacDbContext> _eacDbContextOtions;
        public EacApplicationService(DbContextOptions<EacDbContext> eacDbContextOtions)
        {
            _eacDbContextOtions = eacDbContextOtions;
        }

        public EacApplicationDto GetInfo(string appName)
        {
            using (var _dbContext = new EacDbContext(null, _eacDbContextOtions))
            {
                var appDto = new EacApplicationDto();
                var app = _dbContext.Applications
                    .Include(x => x.Modules).ThenInclude(y => y.Permissions)
                    .Include(x => x.Modules).ThenInclude(y => y.Roles)
                    .Include(x=>x.Modules).ThenInclude(y=>y.ResourceType)
                    .SingleOrDefault(x => x.Id == appName);

                if (app == null || app.Status != Core.Applications.ApplicationStatus.Enabled)
                {
                    throw new ArgumentNullException("Application", "There is no application named '" + appName + "'");
                }
                appDto.Title = app.Title;
                appDto.Name = app.Id;
                foreach (var module in app.Modules)
                {
                    var moduleDto = new EacModuleDto();
                    moduleDto.Name = module.Name;
                    moduleDto.Title = module.Title;
                    moduleDto.ResourceName = module.ResourceType?.Name;
                    moduleDto.ResourceTitle = module.ResourceType?.Title;
                    var permlist = new List<string>();
                    foreach (var permission in module.Permissions.Where(x => x.IsActive))
                    {
                        permlist.Add(permission.Name);
                    }
                    moduleDto.Permissions = permlist;
                    appDto.Modules.Add(moduleDto);
                }
                return appDto;
            }

        }

        public void Sync(EacApplicationInputDto input)
        {
            using (var _dbContext = new EacDbContext(null, _eacDbContextOtions))
            {
                var app = _dbContext.Applications
                    .Include(x => x.Modules).ThenInclude(y => y.Permissions)
                    .Include(x => x.Modules).ThenInclude(y => y.Roles)
                    .SingleOrDefault(x => x.Id == input.Name);
                if (app == null || app.Status != Core.Applications.ApplicationStatus.Enabled)
                {
                    throw new ArgumentNullException("Application", "There is no application named '" + input.Name + "'");
                }

                foreach (var inputModule in input.Modules)
                {
                    var exist = app.Modules.SingleOrDefault(x => x.Name == inputModule.Name);
                    if (exist != null)
                    {
                        SyncPermissions(inputModule.Permissions, exist, _dbContext);
                        exist.IsActive = true;
                    }
                    else
                    {
                        CreateModule(app, inputModule, _dbContext);
                    }
                }

                // Disable doesn't exist in input
                foreach (var module in app.Modules.Where(x =>
                    x.IsActive && !input.Modules.Any(y => y.Name == x.Name)))
                {
                    module.IsActive = false;
                }

                _dbContext.SaveChanges();
            }
        }

        private void CreateModule(Core.Applications.Application app, EacModuleInputDto inputModule, EacDbContext _dbContext)
        {
            var newModule = new Module();
            newModule.Application = app;
            newModule.IsActive = true;
            newModule.Name = inputModule.Name;
            newModule.ResourceType = _dbContext.ResourceTypes.FirstOrDefault(x => x.Name == inputModule.ResourceTypeName);
            newModule.CreatedBy = "SYSTEM";
            foreach (var newPerm in inputModule.Permissions)
            {
                var perm = new Permission();
                perm.IsActive = true;
                perm.Name = newPerm.Name;
                perm.Module = newModule;
                perm.CreatedBy = "SYSTEM";
                newModule.Permissions.Add(perm);
            }
            _dbContext.Modules.Add(newModule);
        }

        private void SyncPermissions(IList<EacPermissionInputDto> newPerms, Module module, EacDbContext _dbContext)
        {
            // Disable doesn't exist in input
            var requireDisableList = module.Permissions.Where(x => x.IsActive && !newPerms.Any(y => y.Name == x.Name)).ToList();
            foreach (var requireDisable in requireDisableList)
            {
                requireDisable.IsActive = false;
            }
            foreach (var newPerm in newPerms)
            {
                var exist = module.Permissions.SingleOrDefault(x => x.Name == newPerm.Name);
                if(exist == null)
                {
                    var perm = new Permission();
                    perm.IsActive = true;
                    perm.Module = module;
                    perm.Name = newPerm.Name;
                    perm.CreatedBy = "SYSTEM";
                    module.Permissions.Add(perm);
                }
                else if (!exist.IsActive)
                {
                    exist.IsActive = true;
                }
            }
        }
    }
}
