﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnterpriseAccessControl.Attributes
{
    public class EacModuleAttribute : Attribute
    {
        public string Name { get; set; }
        public string Schemes { get; set; }
        public string ActionType { get; set; }
        public EacModuleAttribute(string name, string schemes = null, string actionType = "API")
        {
            Schemes = schemes;
            Name = name;
            ActionType = actionType;
        }
    }
}
