﻿using System;

namespace EnterpriseAccessControl.Attributes
{
    public class EacPermissionsAttribute : Attribute
    {
        public string Module { get; set; }
        public EacPermissionsAttribute(string module)
        {
            Module = module;
        }
    }
}
