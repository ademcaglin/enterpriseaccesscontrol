﻿using Microsoft.AspNetCore.Builder;
using System.Collections.Generic;
using System.Reflection;

namespace EnterpriseAccessControl
{
    public class EacOptions
    {
        private List<EacOptionsModule> _modules = new List<EacOptionsModule>();

        public string ApplicationName { get; set; }

        public Assembly ApplicationAssembly { get; set; }

        public IEnumerable<EacOptionsModule> Modules => _modules;

        public void AddModule(string name, string resourceTypeName = null)
        {
            if (string.IsNullOrEmpty(resourceTypeName))
                _modules.Add(new EacOptionsModule(name));
            else
                _modules.Add(new EacOptionsModule(name, resourceTypeName));
        }

        public FullMvcOptions FullMvcOptions { get; set; }
    }

    public class EacOptionsModule
    {
        public string Name { get; set; }

        public string ResourceTypeName { get; set; }

        public string HomePath { get; set; }

        public EacOptionsModule(string name)
        {
            Name = name;
            HomePath = "/" + name.ToLowerInvariant();
        }

        public EacOptionsModule(string name, string resourceTypeName)
            : this(name)
        {
            ResourceTypeName = resourceTypeName;
        }
    }

    public class FullMvcOptions
    {
        public string ModuleCookieName { get; set; } = "EacModule.Cookie";

        public IDictionary<string, string> ProtectedPaths = new Dictionary<string, string>();
        //public string ModuleStaticFilesName { get; set; } = "apps";

        public OpenIdConnectOptions OpenIdConnectOptions { get; set; }

        public string TokenEndpoint { get; set; } = "/module_token";

        public string ChangeModuleEndpoint { get; set; } = "/change_module";
    }
}
