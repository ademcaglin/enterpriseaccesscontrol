﻿using EnterpriseAccessControl.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using System.Reflection;
using Microsoft.AspNetCore.Mvc.Controllers;
using EnterpriseAccessControl.Attributes;
using Microsoft.Extensions.Options;
using EnterpriseAccessControl.Extensions;

namespace EnterpriseAccessControl.Mvc
{
    internal class EacFilterProvider : IFilterProvider
    {
        private readonly IOptions<EacOptions> _eacApp;
        public EacFilterProvider(IOptions<EacOptions> eacApp)
        {
            _eacApp = eacApp;
        }
        public int Order
        {
            get
            {
                // This value is fine. The `DefaultFilterProvider` has an order of `-1000` and so is run before this provider.
                return 0;
            }
        }

        public void OnProvidersExecuted(FilterProviderContext context)
        {
        }

        public void OnProvidersExecuting(FilterProviderContext context)
        {
            var ctrl = context.ActionContext.ActionDescriptor as ControllerActionDescriptor;
            var authorizeAttr = ctrl.MethodInfo.GetCustomAttribute<AuthorizeAttribute>();
            if (authorizeAttr != null) return;

            var moduleAttr = ctrl.MethodInfo.GetCustomAttribute<EacModuleAttribute>();
            if (moduleAttr == null)
            {
                moduleAttr = ctrl.ControllerTypeInfo.GetCustomAttribute<EacModuleAttribute>();
            }
            if (moduleAttr == null) return;
            var eacUser = context.ActionContext.HttpContext.GetEacUser();
            if (moduleAttr.ActionType == "API")
            {
                eacUser.CurrentModule = context.ActionContext.HttpContext.GetCurrentModuleFromHeader();
            }

            var modulePolicy = new AuthorizationPolicyBuilder()
                     .RequireAuthenticatedUser()
                     .AddRequirements(new EacModuleRequirement(eacUser.CurrentModule.Name));
            modulePolicy.AuthenticationSchemes.Add(moduleAttr.Schemes);
            var moduleFilter = new AuthorizeFilter(modulePolicy.Build());
            context.Results.Insert(0, new FilterItem(new FilterDescriptor(moduleFilter, FilterScope.Action), moduleFilter));

            // action auth
            var actionPolicy = new AuthorizationPolicyBuilder()
                 .AddRequirements(new EacPermissionRequirement(Helper.GetPermissionName(eacUser.CurrentModule.Name, ctrl)));
            var actionFilter = new AuthorizeFilter(actionPolicy.Build());
            context.Results.Insert(2, new FilterItem(new FilterDescriptor(actionFilter, FilterScope.Action), actionFilter));
        }

    }
}
