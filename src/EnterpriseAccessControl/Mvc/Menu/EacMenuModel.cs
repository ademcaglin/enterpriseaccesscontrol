﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnterpriseAccessControl.Mvc.Menu
{
    public class EacMenuModel
    {
        //public string ModuleName { get; set; }

        //public string ResourceValue { get; set; }

        public string Path { get; set; }

        public string Title { get; set; }

        public bool Selected { get; set; }
    }
}
