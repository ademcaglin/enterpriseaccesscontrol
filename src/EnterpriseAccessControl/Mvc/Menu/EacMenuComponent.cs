﻿using EnterpriseAccessControl.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnterpriseAccessControl.Mvc.Menu
{
    public class EacMenuComponent : ViewComponent
    {
        private readonly IOptions<EacOptions> _eacApp;
        public EacMenuComponent(IOptions<EacOptions> eacApp)
        {
            _eacApp = eacApp;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var list = new List<EacMenuModel>();
            foreach (var module in HttpContext.GetEacUser().Modules)
            {
                //var moduleOpt = _eacApp.Value.Modules.FirstOrDefault(x => x.Name == module.Name);
                var m = new EacMenuModel()
                {
                    Path = _eacApp.Value.FullMvcOptions.ChangeModuleEndpoint + "/?moduleName=" + module.Name.ToLowerInvariant(),
                    Title = module.Title,
                    Selected = module.Name == HttpContext.GetModuleNameFromCookie()
                };
                if (module.ResourceValue != null)
                {
                    m.Title += "(" + module.ResourceTitle + ")";
                    m.Path += "&resourceValue=" + module.ResourceValue; 
                }
                list.Add(m);
            }
            return View(list);
        }
    }
}
