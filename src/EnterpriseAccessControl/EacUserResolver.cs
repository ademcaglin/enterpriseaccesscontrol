﻿using EnterpriseAccessControl.Core;
using Microsoft.AspNetCore.Http;
using EnterpriseAccessControl.Extensions;
using System.Threading.Tasks;

namespace EnterpriseAccessControl
{
    internal class EacUserResolver : IEacUserResolver
    {
        public readonly IHttpContextAccessor _httpContextAccessor;

        public EacUserResolver(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<EacUserDto> GetUser()
        {
            return _httpContextAccessor.HttpContext.GetEacUser();
        }
    }
}
