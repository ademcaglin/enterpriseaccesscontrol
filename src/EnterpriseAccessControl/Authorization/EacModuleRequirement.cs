﻿using Microsoft.AspNetCore.Authorization;

namespace EnterpriseAccessControl.Authorization
{
    internal class EacModuleRequirement : IAuthorizationRequirement
    {
        public EacModuleRequirement(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
    }
}
