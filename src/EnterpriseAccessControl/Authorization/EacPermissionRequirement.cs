﻿using Microsoft.AspNetCore.Authorization;

namespace EnterpriseAccessControl.Authorization
{
    internal class EacPermissionRequirement : IAuthorizationRequirement
    {
        public EacPermissionRequirement(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
    }
}
