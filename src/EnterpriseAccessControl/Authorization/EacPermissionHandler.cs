﻿using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using EnterpriseAccessControl.Extensions;

namespace EnterpriseAccessControl.Authorization
{
    internal class EacPermissionHandler : AuthorizationHandler<EacPermissionRequirement>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public EacPermissionHandler(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, EacPermissionRequirement requirement)
        {
            if (context.User.Identity.IsAuthenticated)
            {
                var eacUser = _httpContextAccessor.HttpContext.GetEacUser();

                var hasPermission = eacUser.CurrentModule.Permissions
                    .Contains(requirement.Name);
                if (hasPermission)
                {
                    context.Succeed(requirement);
                }
                else
                {
                    context.Fail();
                }
            }
            return Task.FromResult(0);
        }
    }
}
