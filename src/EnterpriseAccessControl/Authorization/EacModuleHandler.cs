﻿using EnterpriseAccessControl.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System.Linq;
using System.Threading.Tasks;

namespace EnterpriseAccessControl.Authorization
{
    internal class EacModuleHandler : AuthorizationHandler<EacModuleRequirement>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        public EacModuleHandler(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, EacModuleRequirement requirement)
        {
            if (context.User.Identity.IsAuthenticated)
            {
                var user = _httpContextAccessor.HttpContext.GetEacUser();
                var hasPermission = user.CurrentModule?.Name == requirement.Name;
                
                if (hasPermission)
                {
                    context.Succeed(requirement);
                }
            }
            return Task.FromResult(0);
        }
    }
}
