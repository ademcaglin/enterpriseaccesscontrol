﻿using EnterpriseAccessControl.Authorization;
using EnterpriseAccessControl.Core;
using EnterpriseAccessControl.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Options;

namespace EnterpriseAccessControl.Extensions
{
    public static class EacServiceCollectionExtension
    {
        public static void AddEac(this IServiceCollection services)
        {
            services.AddSingleton<IAuthorizationHandler, EacPermissionHandler>();
            services.AddSingleton<IAuthorizationHandler, EacModuleHandler>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<IEacUserResolver, EacUserResolver>();
            services.TryAddEnumerable(ServiceDescriptor.Singleton<IFilterProvider, EacFilterProvider>());
            services.AddAuthorization(options =>
            {
                var provider = services.BuildServiceProvider();
                var eacOptions = provider.GetService<IOptions<EacOptions>>();
                var appService = provider.GetService<IEacApplicationService>();
                var app = appService.GetInfo(eacOptions.Value.ApplicationName);
                SavePolicies(app, options);
            });
        }

        private static void SavePolicies(EacApplicationDto app, AuthorizationOptions options)
        {
            foreach (var module in app.Modules)
            {
                foreach (var policyName in module.Permissions)
                {
                    options.AddPolicy(policyName, policy =>
                    {
                        policy.Requirements.Add(new EacPermissionRequirement(policyName));
                    });
                }
            }
        }
    }
}
