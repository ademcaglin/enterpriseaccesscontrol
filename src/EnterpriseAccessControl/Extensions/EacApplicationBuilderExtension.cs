﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;
using EnterpriseAccessControl.Core;
using Microsoft.AspNetCore.Authorization;
using EnterpriseAccessControl.Authorization;
using System.Security.Claims;
using System.Linq;
using System.Reflection;
using EnterpriseAccessControl.Attributes;
using Microsoft.AspNetCore.Http;
using IdentityModel.Client;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace EnterpriseAccessControl.Extensions
{
    public static class EacApplicationBuilderExtension
    {
        public static void UseEac(this IApplicationBuilder app)
        {
            var actionDescriptorCollectionProvider = app.ApplicationServices.GetRequiredService<IActionDescriptorCollectionProvider>();
            var options = app.ApplicationServices.GetRequiredService<IOptions<EacOptions>>().Value;
            var appInputDto = GetApplicationDtoFromActions(options, actionDescriptorCollectionProvider);
            var appService = app.ApplicationServices.GetService<IEacApplicationService>();
            appService.Sync(appInputDto);
            if (options.FullMvcOptions != null)
            {
                app.MapWhen(x => x.Request.Path.StartsWithSegments(options.FullMvcOptions.ChangeModuleEndpoint), builder =>
                 {
                     builder.UseCookieAuthentication(new CookieAuthenticationOptions
                     {
                         AuthenticationScheme = "Cookies",
                         Events = new CookieAuthenticationEvents()
                         {
                             OnValidatePrincipal = async (context) =>
                             {
                                 context.ShouldRenew = true;
                                 var moduleName = context.Request.Query[EacDefaults.ModuleName].FirstOrDefault().ToUpperInvariant();
                                 var moduleClaim = new Claim(EacDefaults.EacModuleNameClaimName, moduleName);
                                 var claim = new Claim(EacDefaults.EacResourceValueClaimName,
                                     context.Request.Query[EacDefaults.ResourceValue].FirstOrDefault());
                                 var p = context.Principal.Clone();
                                 var id = (ClaimsIdentity)p.Identity;
                                 id.AddClaim(claim);
                                 id.AddClaim(moduleClaim);
                                 context.ReplacePrincipal(p);
                                 //var module = options.Modules.FirstOrDefault(x => x.Name == moduleName);
                                 
                                 await Task.CompletedTask;
                             }
                         }
                     });
                     builder.Run(async (context) =>
                     {
                         context.Response.Redirect("/management");
                         await Task.CompletedTask;
                     });
                 });
                app.UseCookieAuthentication(new CookieAuthenticationOptions
                {
                    AuthenticationScheme = "Cookies"
                });
                app.UseOpenIdConnectAuthentication(options.FullMvcOptions.OpenIdConnectOptions);
                app.Map(options.FullMvcOptions.TokenEndpoint, tokenBuilder =>
                {
                    tokenBuilder.Run(async (context) =>
                    {
                        TokenResponse response = null;
                        var refreshToken = await context.Authentication.GetTokenAsync("refresh_token");
                        var tokenClient = new TokenClient(options.FullMvcOptions.OpenIdConnectOptions.Authority + "/connect/token", "mvc.hybrid", "secret");
                        response = await tokenClient.RequestRefreshTokenAsync(refreshToken);
                        context.Response.ContentType = "application/json";
                        context.Response.StatusCode = 200;
                        var result = new
                        {
                            token = response.AccessToken,
                            expiry = response.ExpiresIn,
                            issueDate = DateTime.UtcNow,
                            moduleName = context.GetModuleNameFromCookie(),
                            resourceValue = context.GetResourceValueFromCookie()
                        };
                        await context.Response.WriteAsync(JsonConvert.SerializeObject(result));
                    });
                });

                // To set eacuser
                app.Use(async (context, next) =>
                {
                    if (context.User.Identity.IsAuthenticated)
                    {
                        var input = new EacUserInfoInput();
                        input.ApplicationName = options.ApplicationName;
                        input.UserId = context.User.FindFirst(ClaimTypes.NameIdentifier).Value;
                        var userService = context.RequestServices.GetRequiredService<IEacUserService>();
                        var encResult = userService.GetUserInfo(input).Result;
                        if (encResult.Success)
                        {
                            context.SetEacUser(encResult);
                        }
                    }
                    await next();
                });

                // To protect paths
                app.Use(async (context, next) =>
                {
                    var protectedPath = options.FullMvcOptions.ProtectedPaths
                        .FirstOrDefault(x => context.Request.Path.StartsWithSegments(x.Key));
                    if (protectedPath.Value == null)
                        return;
                    var moduleName = context.GetModuleNameFromCookie();
                    if (moduleName != null)
                    {
                        if (!context.User.Identity.IsAuthenticated)
                        {
                            context.Response.StatusCode = 401;
                            return;
                        }
                        var moduleAuthSuccess = false;

                        var modulepolicy = new AuthorizationPolicyBuilder()
                          .AddRequirements(new EacModuleRequirement(moduleName))
                          .Build();
                        var authService = context.RequestServices.GetRequiredService<IAuthorizationService>();
                        moduleAuthSuccess = await authService.AuthorizeAsync(context.User, modulepolicy);
                        if (!moduleAuthSuccess)
                        {
                            context.Response.StatusCode = 403;
                            return;
                        }
                    }
                    await next();
                });
            }
        }

        private static EacApplicationInputDto GetApplicationDto(EacOptions appliction)
        {
            var applicationInputDto = new EacApplicationInputDto();
            applicationInputDto.Name = appliction.ApplicationName;
            var permissionTypes = appliction.ApplicationAssembly.GetTypes().Where(x =>
                    x.GetTypeInfo().GetCustomAttribute<EacPermissionsAttribute>() != null);
            foreach (var module in appliction.Modules)
            {
                var moduleInputDto = new EacModuleInputDto();
                moduleInputDto.Name = module.Name;
                moduleInputDto.ResourceTypeName = module.ResourceTypeName;

                foreach (var type in permissionTypes.Where(x =>
                    x.GetTypeInfo().GetCustomAttribute<EacPermissionsAttribute>().Module == module.Name))
                {
                    var fields = type.GetFields(BindingFlags.Static | BindingFlags.Public).Where(f => f.IsLiteral);
                    foreach (var field in fields)
                    {
                        var name = field.GetRawConstantValue().ToString();
                        var permissionInput = new EacPermissionInputDto();
                        permissionInput.Name = name;
                        moduleInputDto.Permissions.Add(permissionInput);
                    }
                }
                applicationInputDto.Modules.Add(moduleInputDto);
            }

            return applicationInputDto;
        }

        private static EacApplicationInputDto GetApplicationDtoFromActions(EacOptions appliction, IActionDescriptorCollectionProvider actionDescriptorCollectionProvider)
        {
            var applicationInputDto = new EacApplicationInputDto();
            applicationInputDto.Name = appliction.ApplicationName;
            var permissionTypes = appliction.ApplicationAssembly.GetTypes().Where(x =>
                    x.GetTypeInfo().GetCustomAttribute<EacPermissionsAttribute>() != null);
            foreach (var module in appliction.Modules)
            {
                var moduleInputDto = new EacModuleInputDto();
                moduleInputDto.Name = module.Name;
                moduleInputDto.ResourceTypeName = module.ResourceTypeName;
                var ctrlActions = actionDescriptorCollectionProvider.ActionDescriptors.Items
                    .Where(x => (x as ControllerActionDescriptor)
                    .ControllerTypeInfo.GetCustomAttribute<AreaAttribute>()?.RouteValue == module.Name)
                    .ToList();
                foreach (var action in ctrlActions)
                {

                    var descriptor = action as ControllerActionDescriptor;
                    var excludeAttr = descriptor.MethodInfo.GetCustomAttribute<EacExcludeActionAttribute>();
                    if (excludeAttr == null)
                    {
                        excludeAttr = descriptor.ControllerTypeInfo.GetCustomAttribute<EacExcludeActionAttribute>();
                    }
                    if (excludeAttr == null)
                    {
                        var permissionInput = new EacPermissionInputDto();
                        permissionInput.Name = Helper.GetPermissionName(module.Name, descriptor);
                        moduleInputDto.Permissions.Add(permissionInput);
                    }
                }
                applicationInputDto.Modules.Add(moduleInputDto);
            }


            return applicationInputDto;
        }
    }
}
