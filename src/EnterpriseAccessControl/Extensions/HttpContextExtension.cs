﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Routing.Template;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System.Linq;
using System.Security.Claims;

namespace EnterpriseAccessControl.Extensions
{

    public static class HttpContextExtension
    {
        private const string _UserKey = "EACUSER";
       
        internal static void SetEacUser(this HttpContext context, Core.EacUserDto info)
        {
            context.Items[_UserKey] = info;
        }

        public static Core.EacUserDto GetEacUser(this HttpContext context)
        {
            return (Core.EacUserDto)context.Items[_UserKey];
        }

        

        internal static Core.EacModuleDto GetCurrentModuleFromHeader(this HttpContext context)
        {
            var options = context.RequestServices.GetRequiredService<IOptions<EacOptions>>().Value;
            var moduleName = context.Request.Headers[EacDefaults.EacModuleNameClaimName];
            var eacModule = options.Modules.FirstOrDefault(x => x.Name == moduleName);
            if (eacModule != null)
            {
                var eacUser = context.GetEacUser();
                if (eacModule.ResourceTypeName != null)
                {
                    var resourceValue = context.Request.Headers[EacDefaults.EacResourceValueClaimName]; ;
                    return eacUser.Modules.FirstOrDefault(x =>
                       x.Name == moduleName &&
                       x.ResourceValue == resourceValue);
                }
                return eacUser.Modules.FirstOrDefault(x => x.Name == moduleName);
            }
            return null;
        }

        internal static string GetResourceValueFromCookie(this HttpContext context)
        {
            var options = context.RequestServices.GetRequiredService<IOptions<EacOptions>>().Value;
            var str = context.Request.Cookies[options.FullMvcOptions.ModuleCookieName];
            return str != null && str.Split(';').Count() == 2 ? str.Split(';')[1] : null;
        }

        internal static string GetModuleNameFromCookie(this HttpContext context)
        {
            return context.User.FindFirst(EacDefaults.EacModuleNameClaimName)?.Value;
            //var options = context.RequestServices.GetRequiredService<IOptions<EacOptions>>().Value;
            //var str = context.Request.Cookies[options.FullMvcOptions.ModuleCookieName];
            
            //return str == null? null : str.Split(';').FirstOrDefault();
        }

    }
}
