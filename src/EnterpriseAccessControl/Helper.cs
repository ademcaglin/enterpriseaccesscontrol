﻿using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Routing.Template;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnterpriseAccessControl
{
    internal class Helper
    {
        public static string GetPermissionName(string module, ControllerActionDescriptor descriptor)
        {
            var actionName = descriptor.MethodInfo.Name.ToUpperInvariant();
            var ctrlName = descriptor.ControllerName.ToUpperInvariant();
            var permName = module  + "-" + ctrlName + "-" + actionName;
            return permName;

        }
    }
}
