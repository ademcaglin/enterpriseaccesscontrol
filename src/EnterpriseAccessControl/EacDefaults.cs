﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnterpriseAccessControl
{
    public class EacDefaults
    {
        public const string ModuleName = "moduleName";

        public const string ResourceValue = "resourceValue";

        public const string EacResourceValueClaimName = "EACRESOURCEVALUE";

        public const string EacModuleNameClaimName = "EACMODULENAME";

    }
}
